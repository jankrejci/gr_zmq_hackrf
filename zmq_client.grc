options:
  parameters:
    author: ''
    category: '[GRC Hier Blocks]'
    cmake_opt: ''
    comment: ''
    copyright: ''
    description: ''
    gen_cmake: 'On'
    gen_linking: dynamic
    generate_options: qt_gui
    hier_block_src_path: '.:'
    id: zmq_client
    max_nouts: '0'
    output_language: python
    placement: (0,0)
    qt_qss_theme: ''
    realtime_scheduling: ''
    run: 'True'
    run_command: '{python} -u {filename}'
    run_options: run
    sizing_mode: fixed
    thread_safe_setters: ''
    title: ZMQ Client
    window_size: ''
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [8, 8]
    rotation: 0
    state: enabled

blocks:
- name: center_freq
  id: variable_qtgui_range
  parameters:
    comment: ''
    gui_hint: 1,6,1,1
    label: Center frequency
    min_len: '200'
    orient: Qt.Horizontal
    rangeType: float
    start: 850 * 1000000
    step: 1 * 1000000
    stop: 920 * 1000000
    value: 872 * 1000000
    widget: counter_slider
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [176, 8.0]
    rotation: 0
    state: true
- name: intensity_max
  id: variable_qtgui_range
  parameters:
    comment: ''
    gui_hint: 8,6,1,1
    label: ''
    min_len: '200'
    orient: Qt.Vertical
    rangeType: float
    start: '-70'
    step: '1'
    stop: '0'
    value: '-55'
    widget: dial
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [512, 8.0]
    rotation: 0
    state: enabled
- name: intensity_min
  id: variable_qtgui_range
  parameters:
    comment: ''
    gui_hint: 9,6,1,1
    label: intensity_min
    min_len: '200'
    orient: Qt.Vertical
    rangeType: float
    start: '-120'
    step: '1'
    stop: '0'
    value: '-95'
    widget: dial
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [640, 8.0]
    rotation: 0
    state: enabled
- name: ip_address
  id: variable
  parameters:
    comment: ''
    value: '"192.168.1.185"'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [784, 12.0]
    rotation: 0
    state: true
- name: samp_rate
  id: variable_qtgui_range
  parameters:
    comment: ''
    gui_hint: 2,6,1,1
    label: Sample rate
    min_len: '200'
    orient: Qt.Horizontal
    rangeType: float
    start: 8*1000000
    step: 1 * 1000000
    stop: 20*1000000
    value: 20*1000000
    widget: counter_slider
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [360, 8.0]
    rotation: 0
    state: true
- name: blocks_throttle_0
  id: blocks_throttle
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    ignoretag: 'True'
    maxoutbuf: '0'
    minoutbuf: '0'
    samples_per_second: samp_rate
    type: complex
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [296, 412.0]
    rotation: 0
    state: true
- name: fosphor_qt_sink_c_0
  id: fosphor_qt_sink_c
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    freq_center: center_freq
    freq_span: samp_rate
    gui_hint: tab@1
    maxoutbuf: '0'
    minoutbuf: '0'
    wintype: firdes.WIN_RECTANGULAR
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [504, 564.0]
    rotation: 0
    state: true
- name: qtgui_freq_sink_x_0
  id: qtgui_freq_sink_x
  parameters:
    affinity: ''
    alias: ''
    alpha1: '1.0'
    alpha10: '1.0'
    alpha2: '1.0'
    alpha3: '1.0'
    alpha4: '1.0'
    alpha5: '1.0'
    alpha6: '1.0'
    alpha7: '1.0'
    alpha8: '1.0'
    alpha9: '1.0'
    autoscale: 'False'
    average: '1.0'
    axislabels: 'True'
    bw: samp_rate
    color1: '"blue"'
    color10: '"dark blue"'
    color2: '"red"'
    color3: '"green"'
    color4: '"black"'
    color5: '"cyan"'
    color6: '"magenta"'
    color7: '"yellow"'
    color8: '"dark red"'
    color9: '"dark green"'
    comment: ''
    ctrlpanel: 'False'
    fc: center_freq
    fftsize: '1024'
    freqhalf: 'True'
    grid: 'True'
    gui_hint: tab@0:3,1,1,3
    label: Relative Gain
    label1: ''
    label10: ''''''
    label2: ''''''
    label3: ''''''
    label4: ''''''
    label5: ''''''
    label6: ''''''
    label7: ''''''
    label8: ''''''
    label9: ''''''
    legend: 'False'
    maxoutbuf: '0'
    minoutbuf: '0'
    name: '""'
    nconnections: '1'
    showports: 'False'
    tr_chan: '0'
    tr_level: '0.0'
    tr_mode: qtgui.TRIG_MODE_FREE
    tr_tag: '""'
    type: complex
    units: dB
    update_time: '0.1'
    width1: '1'
    width10: '1'
    width2: '1'
    width3: '1'
    width4: '1'
    width5: '1'
    width6: '1'
    width7: '1'
    width8: '1'
    width9: '1'
    wintype: firdes.WIN_BLACKMAN_hARRIS
    ymax: intensity_max
    ymin: intensity_min
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [504, 408.0]
    rotation: 0
    state: enabled
- name: qtgui_waterfall_sink_x_0
  id: qtgui_waterfall_sink_x
  parameters:
    affinity: ''
    alias: ''
    alpha1: '1.0'
    alpha10: '1.0'
    alpha2: '1.0'
    alpha3: '1.0'
    alpha4: '1.0'
    alpha5: '1.0'
    alpha6: '1.0'
    alpha7: '1.0'
    alpha8: '1.0'
    alpha9: '1.0'
    axislabels: 'True'
    bw: samp_rate
    color1: '0'
    color10: '0'
    color2: '0'
    color3: '0'
    color4: '0'
    color5: '0'
    color6: '0'
    color7: '0'
    color8: '0'
    color9: '0'
    comment: ''
    fc: center_freq
    fftsize: 1024*32
    freqhalf: 'True'
    grid: 'True'
    gui_hint: tab@0:1,1,2,3
    int_max: intensity_max
    int_min: intensity_min
    label1: ''
    label10: ''
    label2: ''
    label3: ''
    label4: ''
    label5: ''
    label6: ''
    label7: ''
    label8: ''
    label9: ''
    legend: 'False'
    maxoutbuf: '0'
    minoutbuf: '0'
    name: '""'
    nconnections: '1'
    showports: 'False'
    type: complex
    update_time: '0.1'
    wintype: firdes.WIN_RECTANGULAR
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [504, 304.0]
    rotation: 0
    state: enabled
- name: tab
  id: qtgui_tab_widget
  parameters:
    alias: ''
    comment: ''
    gui_hint: 1,1,10,5
    label0: Tab 0
    label1: Tab 1
    label10: Tab 10
    label11: Tab 11
    label12: Tab 12
    label13: Tab 13
    label14: Tab 14
    label15: Tab 15
    label16: Tab 16
    label17: Tab 17
    label18: Tab 18
    label19: Tab 19
    label2: Tab 2
    label3: Tab 3
    label4: Tab 4
    label5: Tab 5
    label6: Tab 6
    label7: Tab 7
    label8: Tab 8
    label9: Tab 9
    num_tabs: '2'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [8, 136.0]
    rotation: 0
    state: true
- name: xmlrpc_client_0
  id: xmlrpc_client
  parameters:
    addr: '''+ self.ip_address +'''
    alias: ''
    callback: set_center_freq
    comment: ''
    port: '2001'
    variable: center_freq
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [176, 136.0]
    rotation: 0
    state: true
- name: xmlrpc_client_0_0
  id: xmlrpc_client
  parameters:
    addr: '''+ self.ip_address +'''
    alias: ''
    callback: set_samp_rate
    comment: ''
    port: '2001'
    variable: samp_rate
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [360, 136.0]
    rotation: 0
    state: true
- name: zeromq_sub_source_0
  id: zeromq_sub_source
  parameters:
    address: tcp://192.168.1.185:2000
    affinity: ''
    alias: ''
    comment: ''
    hwm: '-1'
    maxoutbuf: '0'
    minoutbuf: '0'
    pass_tags: 'False'
    timeout: '100'
    type: complex
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [56, 396.0]
    rotation: 0
    state: true

connections:
- [blocks_throttle_0, '0', fosphor_qt_sink_c_0, '0']
- [blocks_throttle_0, '0', qtgui_freq_sink_x_0, '0']
- [blocks_throttle_0, '0', qtgui_waterfall_sink_x_0, '0']
- [zeromq_sub_source_0, '0', blocks_throttle_0, '0']

metadata:
  file_format: 1
